﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Simulation_of_Life
{
    class Personnage
    {
        private string _prenom;
        private string _nom;
        private int _age = 32;
        private int _faim = 100;
        private int _soif = 100;
        private int _energie = 100;
        private int _sante = 100;
        private int _bonheur = 100;
        private int _hygiene = 100;
        private double _argent = 650;

        public string Prenom
        {
            get
            {
                return _prenom;
            }

            set
            {
                _prenom = value;
            }
        }

        public string Nom
        {
            get
            {
                return _nom;
            }

            set
            {
                _nom = value;
            }
        }

        public int Faim
        {
            get
            {
                return _faim;
            }

            set
            {
                _faim = value;
            }
        }

        public int Soif
        {
            get
            {
                return _soif;
            }

            set
            {
                _soif = value;
            }
        }

        public int Energie
        {
            get
            {
                return _energie;
            }

            set
            {
                _energie = value;
            }
        }

        public int Sante
        {
            get
            {
                return _sante;
            }

            set
            {
                _sante = value;
            }
        }

        public double Argent
        {
            get
            {
                return _argent;
            }

            set
            {
                _argent = value;
            }
        }

        public int Age
        {
            get
            {
                return _age;
            }

            set
            {
                _age = value;
            }
        }

        public int Bonheur
        {
            get
            {
                return _bonheur;
            }

            set
            {
                _bonheur = value;
            }
        }

        public int Hygiene
        {
            get
            {
                return _hygiene;
            }

            set
            {
                _hygiene = value;
            }
        }
    }
}
