﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Simulation_of_Life
{
    class Program
    {
        static void Main(string[] args)
        {
            #region Déclaration des variables
            Personnage personnage = new Personnage();
            Metier metier = new Metier();
            System.Diagnostics.Stopwatch stopwatch = new System.Diagnostics.Stopwatch();

            int dir = 0;

            #region Actions
            Actions _AllerAuMagasin = new Actions()
            {
                Action = "Shopping",
                CoutEnArgent = 0,
                CoutEnEnergie = 15,
                TempsDeLAction = 90
            };
            Actions _Travailler = new Actions()
            {
                Action = "Travailler",
                CoutEnArgent = 0,
                CoutEnEnergie = metier.CoutEnEnergie,
                TempsDeLAction = 480
            };
            Actions _SeLaver = new Actions()
            {
                Action = "Se laver",
                CoutEnArgent = 0,
                CoutEnEnergie = 5,
                TempsDeLAction = 45,
                RapportBonheur = 10
            };
            Actions d = new Actions()
            {
                Action = "",
                CoutEnArgent = 0,
                CoutEnEnergie = 0,
                TempsDeLAction = 0
            };
            Actions e = new Actions()
            {
                Action = "",
                CoutEnArgent = 0,
                CoutEnEnergie = 0,
                TempsDeLAction = 0
            };
            Actions f = new Actions()
            {
                Action = "",
                CoutEnArgent = 0,
                CoutEnEnergie = 0,
                TempsDeLAction = 0
            };
            #endregion

            #region Produits
            Produit cocaCola = new Produit("Coca-Cola", 2.3, 2, 8, 5, 5);
            Produit redBull = new Produit("Red Bull", 3.2, 2, 6, 5, 10);
            Produit eau = new Produit("Eau", 0.5, 0, 15, 2, 0);
            Produit iceTea = new Produit("Ice-Tea", 2.5, 3, 8, 5, 5);
            Produit monster = new Produit("Monster", 5.5, 3, 12, 7, 15);
            Produit cafe = new Produit("Café", 4, 3, 5, 5, 15);
            Produit the = new Produit("Thé", 1.2, 2, 10, 5, 0);
            Produit vin = new Produit("Vin", 6, -5, 3, 12, -7);
            Produit biere = new Produit("Bière", 1.5, 2, 2, 10, -7);

            Produit raviolis = new Produit("Raviolis", 4, 10, -5, 2, -5);
            Produit pates = new Produit("Pâtes", 3.5, 15, -8, 2, -8);
            Produit pizza = new Produit("Pizza", 7, 18, -5, 8, -6);
            Produit platPrepare = new Produit("Plat Préparé", 12, 25, -7, 6, -10);
            Produit steakFrites = new Produit("Steak Frites", 22, 35, -5, 14, -8);
            Produit poulet = new Produit("Poulet", 18, 28, -5, 12, -10);
            Produit pain = new Produit("Pain", 2, 8, -2, 3, -2);
            Produit chips = new Produit("Chips", 1.5, 5, -5, 10, -5);
            Produit hamburger = new Produit("Hamburger", 4.5, 10, -5, 13, -8);
            Produit poisson = new Produit("Poisson", 12, 20, -5, 10, -5);
            Produit lasagne = new Produit("Lasagne", 6, 16, -6, 8, -6);
            Produit soupe = new Produit("Soupe", 3, 10, 5, 4, -5);

            List<Produit> ProduitsDisponiblesAuMagasin = new List<Produit>();

            ProduitsDisponiblesAuMagasin.Add(cocaCola);
            ProduitsDisponiblesAuMagasin.Add(redBull);
            ProduitsDisponiblesAuMagasin.Add(eau);
            ProduitsDisponiblesAuMagasin.Add(iceTea);
            ProduitsDisponiblesAuMagasin.Add(monster);
            ProduitsDisponiblesAuMagasin.Add(cafe);
            ProduitsDisponiblesAuMagasin.Add(the);
            ProduitsDisponiblesAuMagasin.Add(vin);
            ProduitsDisponiblesAuMagasin.Add(biere);
            ProduitsDisponiblesAuMagasin.Add(raviolis);
            ProduitsDisponiblesAuMagasin.Add(pates);
            ProduitsDisponiblesAuMagasin.Add(pizza);
            ProduitsDisponiblesAuMagasin.Add(platPrepare);
            ProduitsDisponiblesAuMagasin.Add(steakFrites);
            ProduitsDisponiblesAuMagasin.Add(poulet);
            ProduitsDisponiblesAuMagasin.Add(pain);
            ProduitsDisponiblesAuMagasin.Add(chips);
            ProduitsDisponiblesAuMagasin.Add(hamburger);
            ProduitsDisponiblesAuMagasin.Add(poisson);
            ProduitsDisponiblesAuMagasin.Add(lasagne);
            ProduitsDisponiblesAuMagasin.Add(soupe);

            Dictionary<Produit, int> ContenuFrigo = new Dictionary<Produit, int>();

            ContenuFrigo.Add(eau, 3);
            ContenuFrigo.Add(pizza, 2); 
            #endregion

            //EcranDeJeu();
            Console.SetCursorPosition(0, 0);
            Console.CursorVisible = false;

            bool Jeu = true;
            string EcranActuel = "Menu";
            int choixOption = 0;
            bool creationPersonnage = true; // PAS OUBLIER DE RESET TRUE
            bool animationIntro = true;     // *SKIPPER LA CREATION PERSO ET L'INTRO*
            int Heure = 0;
            int Heure2 = 0;
            int Minutes = 0;
            int Minutes2 = 0;
            int Temps = 330; // MODIFIER L'HEURE
            string zero = "";
            string zero2 = "";
            string zero3 = "";
            int JourActuel = 26;
            int JourDeLaSemaineActuel = 5; // MODIFIER LE JOUR DE LA SEMAINE
            int MoisActuel = 3;
            int echelon = 0;
            double salaireDuMois = 0;
            bool payeRecue = false;
            int compteTour = 0;
            int compteurDeJours = 0;
            int vitesse = -1;
            int vitesseWatch = 8;
            Random nombreAleatoire = new Random();

            ConsoleKeyInfo touche = new ConsoleKeyInfo();

            #region Instructions
            List<string> listeInstructions = new List<string>();
            listeInstructions.Add("Vous allez incarner un personnage qui,");
            listeInstructions.Add("tout au long de sa vie, devra effectuer ");
            listeInstructions.Add("certaines actions comme aller travailler, ");
            listeInstructions.Add("faire ses courses, manger, dormir, ...");
            listeInstructions.Add("Veillez à respecter ses besoins, sinon");
            listeInstructions.Add("son état de santé se dégradera et ça, c'est pas bon signe.");
            listeInstructions.Add("Plus son état de santé est faible, plus il");
            listeInstructions.Add("a de chances qu'une bricole lui arrive. ");
            listeInstructions.Add("Si sa santé tombe à 0, votre personnage meurt ");
            listeInstructions.Add("et c'est le Game Over.");
            listeInstructions.Add("Le but étant de survivre le plus longtemps possible.");
            listeInstructions.Add("Bon jeu ! "); 
            #endregion

            #region Listes de dates
            List<string> JourDeLaSemaine = new List<string>();
            JourDeLaSemaine.Add("LUN");
            JourDeLaSemaine.Add("MAR");
            JourDeLaSemaine.Add("MER");
            JourDeLaSemaine.Add("JEU");
            JourDeLaSemaine.Add("VEN");
            JourDeLaSemaine.Add("SAM");
            JourDeLaSemaine.Add("DIM");

            List<string> Mois = new List<string>();
            Mois.Add("JAN");
            Mois.Add("FÉV");
            Mois.Add("MAR");
            Mois.Add("AVR");
            Mois.Add("MAI");
            Mois.Add("JUN");
            Mois.Add("JUI");
            Mois.Add("AOU");
            Mois.Add("SEP");
            Mois.Add("OCT");
            Mois.Add("NOV");
            Mois.Add("DEC"); 
            #endregion

            #endregion

            void EcranDeJeu()
            {
                for (int i = 0; i < 30; i++)
                {
                    Console.WriteLine("                                                            |");
                }
                Console.WriteLine("------------------------------------------------------------┘");
            }

            void AfficherEmplacementActuel(string emplacement)
            {
                if (("Emplacement Actuel : " + emplacement).Length % 2 == 1)
                {
                    Console.SetCursorPosition((60 - ("Emplacement Actuel : " + emplacement).Length + 1) / 2, 1);
                }

                else
                {
                    Console.SetCursorPosition((60 - ("Emplacement Actuel : " + emplacement).Length) / 2, 1);
                }

                Console.Write("Emplacement Actuel : " + emplacement);
            }

            void EffacerContenu(int debut, int fin)
            {
                for (int i = debut; i < fin; i++)
                {
                    Console.SetCursorPosition(1, i);
                    Console.Write("                                                          ");
                }
            }

            void CaseMenu(int cas, int numero)
            {
                if (cas == numero)
                {
                    Console.BackgroundColor = ConsoleColor.White;
                    Console.ForegroundColor = ConsoleColor.Black;
                }

                else
                {
                    Console.BackgroundColor = ConsoleColor.Black;
                    Console.ForegroundColor = ConsoleColor.DarkGray;
                }
            }
            
            void Magasin( out Personnage perso, out Dictionary<Produit, int> frigo, List<Produit> produitsDuMagasin)
            {
                Magasin magasin = new Magasin();
                perso = personnage;
                frigo = ContenuFrigo;
                string texteAction = $"{perso.Prenom} se rend au magasin";
                string texteAction2 = $"{perso.Prenom} revient du magasin";
                bool shopping = true;

                bool menuShopping = true;
                bool menuCourses = false;
                bool menuPanier = false;
                bool menuPayer = false;

                int c = 0;
                ConsoleKeyInfo touch = new ConsoleKeyInfo();
                double coutPanier = 0;

                EffacerContenu(15, 30);

                if (texteAction.Length % 2 == 1)
                {
                    Console.SetCursorPosition(30 - ((texteAction.Length + 1) / 2), 20);
                }

                else
                {
                    Console.SetCursorPosition(30 - ((texteAction.Length) / 2), 20);
                }

                Console.Write(texteAction);

                System.Threading.Thread.Sleep(3000);

                EffacerContenu(1, 30);

                while (shopping)
                {
                    AfficherEmplacementActuel("Magasin");

                    Console.SetCursorPosition(1, 3);

                    Console.Write($"ARGENT : {perso.Argent} $");

                    coutPanier = 0;

                    foreach (KeyValuePair<Produit, int> p in magasin.Panier)
                    {
                        coutPanier += p.Key.Prix * p.Value;
                    }

                    Console.SetCursorPosition(1, 4);

                    Console.Write($"PANIER : {coutPanier} $");

                    while (menuShopping)
                    {
                        if (c == 0)
                        {
                            Console.BackgroundColor = ConsoleColor.White;
                            Console.ForegroundColor = ConsoleColor.Black;
                        }

                        else
                        {
                            Console.BackgroundColor = ConsoleColor.Black;
                            Console.ForegroundColor = ConsoleColor.DarkGray;
                        }

                        Console.SetCursorPosition(12, 12);
                        Console.WriteLine("         FAIRE SES COURSES          ");

                        if (c == 1)
                        {
                            Console.BackgroundColor = ConsoleColor.White;
                            Console.ForegroundColor = ConsoleColor.Black;
                        }

                        else
                        {
                            Console.BackgroundColor = ConsoleColor.Black;
                            Console.ForegroundColor = ConsoleColor.DarkGray;
                        }

                        Console.SetCursorPosition(12, 15);
                        Console.WriteLine("   AFFICHER LE CONTENU DU PANIER    ");

                        if (c == 2)
                        {
                            Console.BackgroundColor = ConsoleColor.White;
                            Console.ForegroundColor = ConsoleColor.Black;
                        }

                        else
                        {
                            Console.BackgroundColor = ConsoleColor.Black;
                            Console.ForegroundColor = ConsoleColor.DarkGray;
                        }

                        Console.SetCursorPosition(12, 18);
                        Console.WriteLine("          PASSER EN CAISSE          ");

                        touch = Console.ReadKey(true);

                        if ((int)touch.Key == 38)
                        {
                            if (c > 0)
                            {
                                c--;
                            }
                        }

                        else if ((int)touch.Key == 40)
                        {
                            if (c < 2)
                            {
                                c++;
                            }
                        }

                        else if ((int)touch.Key == 13)
                        {
                            menuShopping = false;

                            Console.BackgroundColor = ConsoleColor.Black;
                            Console.ForegroundColor = ConsoleColor.White;

                            if (c == 0)
                            {
                                menuCourses = true;
                            }

                            else if (c == 1)
                            {
                                menuPanier = true;
                            }

                            else if (c == 2)
                            {
                                menuPayer = true;
                            }

                            c = 0;
                        }
                    }

                    EffacerContenu(5, 30);

                    while (menuCourses)
                    {
                        EffacerContenu(14, 21);
                        EffacerContenu(2, 5);

                        Console.ForegroundColor = ConsoleColor.White;

                        Console.SetCursorPosition(1, 3);

                        Console.Write($"ARGENT : {perso.Argent} $");

                        coutPanier = 0;

                        foreach (KeyValuePair<Produit, int> p in magasin.Panier)
                        {
                            coutPanier += p.Key.Prix * p.Value;
                        }

                        Console.SetCursorPosition(1, 4);

                        Console.Write($"PANIER : {coutPanier} $");

                        int petitPanier = 21;

                        EffacerContenu(21, 30);

                        foreach (KeyValuePair<Produit, int> kvp in magasin.Panier)
                        {
                            if (petitPanier < 28)
                            {
                                Console.SetCursorPosition(2, petitPanier);
                            }

                            else if (petitPanier < 35)
                            {
                                Console.SetCursorPosition(22, petitPanier - 7);
                            }

                            else if (petitPanier < 42)
                            {
                                Console.SetCursorPosition(42, petitPanier - 14);
                            }

                            Console.Write($"{kvp.Key.Nom} - {kvp.Value}");

                            petitPanier++;
                        }

                        Console.SetCursorPosition(1, 29);
                        Console.Write("        APPUYEZ SUR [ESCAPE] POUR REVENIR AU MENU ");

                        for (int i = 0; i < produitsDuMagasin.Count; i++)
                        {
                            CaseMenu(c, i);

                            if (i == 0)
                            {
                                Console.SetCursorPosition(2, 6);
                            }

                            else if (i == 1)
                            {
                                Console.SetCursorPosition(22, 6);
                            }

                            else if (i == 2)
                            {
                                Console.SetCursorPosition(42, 6);
                            }

                            else if (i == 3)
                            {
                                Console.SetCursorPosition(2, 7);
                            }

                            else if (i == 4)
                            {
                                Console.SetCursorPosition(22, 7);
                            }

                            else if (i == 5)
                            {
                                Console.SetCursorPosition(42, 7);
                            }

                            else if (i == 6)
                            {
                                Console.SetCursorPosition(2, 8);
                            }

                            else if (i == 7)
                            {
                                Console.SetCursorPosition(22, 8);
                            }

                            else if (i == 8)
                            {
                                Console.SetCursorPosition(42, 8);
                            }

                            else if (i == 9)
                            {
                                Console.SetCursorPosition(2, 9);
                            }

                            else if (i == 10)
                            {
                                Console.SetCursorPosition(22, 9);
                            }

                            else if (i == 11)
                            {
                                Console.SetCursorPosition(42, 9);
                            }

                            else if (i == 12)
                            {
                                Console.SetCursorPosition(2, 10);
                            }

                            else if (i == 13)
                            {
                                Console.SetCursorPosition(22, 10);
                            }

                            else if (i == 14)
                            {
                                Console.SetCursorPosition(42, 10);
                            }

                            else if (i == 15)
                            {
                                Console.SetCursorPosition(2, 11);
                            }

                            else if (i == 16)
                            {
                                Console.SetCursorPosition(22, 11);
                            }

                            else if (i == 17)
                            {
                                Console.SetCursorPosition(42, 11);
                            }

                            else if (i == 18)
                            {
                                Console.SetCursorPosition(2, 12);
                            }

                            else if (i == 19)
                            {
                                Console.SetCursorPosition(22, 12);
                            }

                            else if (i == 20)
                            {
                                Console.SetCursorPosition(42, 12);
                            }

                            Console.WriteLine(produitsDuMagasin[i].Nom);

                            Console.BackgroundColor = ConsoleColor.Black;
                            Console.ForegroundColor = ConsoleColor.White;

                            if (i == c)
                            {
                                Console.SetCursorPosition(22, 14);
                                Console.Write($"Produit : {produitsDuMagasin[i].Nom}");

                                Console.SetCursorPosition(22, 15);
                                Console.Write($"Prix : {produitsDuMagasin[i].Prix} $");

                                Console.SetCursorPosition(22, 16);
                                Console.Write($"Faim : {produitsDuMagasin[i].ImpactFaim}");

                                Console.SetCursorPosition(22, 17);
                                Console.Write($"Soif : {produitsDuMagasin[i].ImpactSoif}");

                                Console.SetCursorPosition(22, 18);
                                Console.Write($"Énergie : {produitsDuMagasin[i].ImpactEnergie}");

                                Console.SetCursorPosition(22, 19);
                                Console.Write($"Bonheur : {produitsDuMagasin[i].ImpactBonheur}");
                            }
                        }

                        touch = Console.ReadKey(true);

                        #region Détection de la touche appuyée
                        if ((int)touch.Key == 37)
                        {
                            if (c > 0)
                            {
                                c--;
                            }
                        }

                        else if ((int)touch.Key == 38)
                        {
                            if (c - 3 >= 0)
                            {
                                c -= 3;
                            }
                        }

                        else if ((int)touch.Key == 39)
                        {
                            if (c + 1 < produitsDuMagasin.Count)
                            {
                                c++;
                            }
                        }

                        else if ((int)touch.Key == 40)
                        {
                            if (c + 3 <= produitsDuMagasin.Count - 1)
                            {
                                c += 3;
                            }
                        }

                        else if ((int)touch.Key == 13)
                        {
                            for (int i = 0; i < produitsDuMagasin.Count; i++)
                            {
                                if (i == c)
                                {
                                    magasin.AjouterAuPanier(produitsDuMagasin[i]);
                                }
                            }
                        }

                        else if ((int)touch.Key == 27)
                        {
                            menuCourses = false;
                            menuShopping = true;
                            c = 0;
                            EffacerContenu(2, 30);
                        } 
                        #endregion
                    }

                    while (menuPanier)
                    {
                        AfficherEmplacementActuel("Magasin");

                        Console.SetCursorPosition(1, 3);

                        Console.Write($"ARGENT : {perso.Argent} $");

                        coutPanier = 0;

                        foreach (KeyValuePair<Produit, int> p in magasin.Panier)
                        {
                            coutPanier += p.Key.Prix * p.Value;
                        }

                        Console.SetCursorPosition(1, 4);

                        Console.Write($"PANIER : {coutPanier} $");

                        Console.SetCursorPosition(1, 28);
                        Console.Write("       APPUYEZ SUR [ENTER] POUR RETIRER UN ARTICLE");

                        Console.SetCursorPosition(1, 29);
                        Console.Write("        APPUYEZ SUR [ESCAPE] POUR REVENIR AU MENU ");

                        for (int i = 0; i < magasin.Panier.Count; i++)
                        {
                            Console.SetCursorPosition(22, (5 + i));
                        }

                        int count = 0;

                        foreach (KeyValuePair<Produit, int> kvp in magasin.Panier)
                        {
                            Console.SetCursorPosition(1, (6 + count));
                            Console.Write("Art. : ");

                            if (c == count)
                            {
                                Console.BackgroundColor = ConsoleColor.White;
                                Console.ForegroundColor = ConsoleColor.Black;
                                Console.Write(kvp.Key.Nom);
                            }

                            else
                            {
                                Console.BackgroundColor = ConsoleColor.Black;
                                Console.ForegroundColor = ConsoleColor.White;
                                Console.Write(kvp.Key.Nom);
                            }

                            Console.BackgroundColor = ConsoleColor.Black;
                            Console.ForegroundColor = ConsoleColor.White;

                            Console.SetCursorPosition((35 - ($"Qty : {kvp.Value}".Length)), (6 + count));
                            Console.Write($"Qty : {kvp.Value}");

                            Console.SetCursorPosition((58 - ($"Total : {kvp.Key.Prix * kvp.Value} $".Length)), (6 + count));
                            Console.Write($"Total : {kvp.Key.Prix * kvp.Value} $");

                            count++;
                        }

                        touch = Console.ReadKey(true);

                        if ((int)touch.Key == 38)
                        {
                            if (c > 0)
                            {
                                c--;
                            }
                        }

                        else if ((int)touch.Key == 40)
                        {
                            if (c < magasin.Panier.Count - 1)
                            {
                                c++;
                            }
                        }

                        else if ((int)touch.Key == 27)
                        {
                            menuPanier = false;
                            menuShopping = true;
                            c = 0;
                            EffacerContenu(2, 30);
                        }

                        int longueurPanier = 0;
                        int longueurPanier2 = 0;

                        if ((int)touch.Key == 13)
                        {
                            longueurPanier = magasin.Panier.Count;

                            if (magasin.Panier.Count > 0)
                            {
                                magasin.RetirerDuPanier(magasin.Panier.ElementAt(c).Key);
                            }

                            longueurPanier2 = magasin.Panier.Count;

                            if (longueurPanier != longueurPanier2)
                            {
                                if (c > 0)
                                {
                                    c--;
                                }
                            }
                        }
                        
                        EffacerContenu(2, 30);
                    }

                    while (menuPayer)
                    {
                        AfficherEmplacementActuel("Caisse");

                        Console.SetCursorPosition(1, 3);

                        Console.Write($"ARGENT : {perso.Argent} $");

                        coutPanier = 0;

                        foreach (KeyValuePair<Produit, int> p in magasin.Panier)
                        {
                            coutPanier += p.Key.Prix * p.Value;
                        }

                        Console.SetCursorPosition(1, 4);

                        Console.Write($"PANIER : {coutPanier} $");

                        Console.SetCursorPosition(1, 13);
                        Console.Write($"                   Votre argent : {perso.Argent} $");

                        Console.SetCursorPosition(1, 15);
                        Console.Write($"                  Montant du panier : {coutPanier} $");

                        Console.SetCursorPosition(1, 17);
                        Console.Write($"                 Après la transaction : {perso.Argent - coutPanier} $");

                        perso.Argent -= coutPanier;

                        Console.SetCursorPosition(23, 22);
                        Console.BackgroundColor = ConsoleColor.White;
                        Console.ForegroundColor = ConsoleColor.Black;

                        Console.Write("   CONFIRMER   ");

                        Console.BackgroundColor = ConsoleColor.Black;
                        Console.ForegroundColor = ConsoleColor.White;

                        Console.SetCursorPosition(1, 29);
                        Console.Write("        APPUYEZ SUR [ESCAPE] POUR REVENIR AU MENU ");

                        touch = Console.ReadKey(true);

                        if ((int)touch.Key == 27)
                        {
                            menuPayer = false;
                            menuShopping = true;
                            c = 0;
                            EffacerContenu(2, 30);
                        }

                        else if ((int)touch.Key == 13)
                        {
                            menuPayer = false;
                            shopping = false;
                            EffacerContenu(0, 30);

                            Console.SetCursorPosition(14, 14);
                            Console.Write("Merci d'avoir fait vos courses");
                            Console.SetCursorPosition(18, 15);
                            Console.Write("chez Technobel shop !");

                            System.Threading.Thread.Sleep(3000);

                            EffacerContenu(0, 30);

                            foreach (KeyValuePair<Produit, int> p in magasin.Panier)
                            {
                                if (frigo.ContainsKey(p.Key))
                                {
                                    frigo[p.Key] += p.Value;
                                }

                                else
                                {
                                    frigo.Add(p.Key, p.Value);
                                }
                            }

                        }
                    }
                }
            }

            void OuvrirLeFrigo(out Dictionary<Produit, int> frigo, out Personnage perso)
            {
                bool frigoOuvert = true;

                int c = 0;

                ConsoleKeyInfo touch = new ConsoleKeyInfo();

                frigo = ContenuFrigo;

                perso = personnage;

                EffacerContenu(0, 30);

                while (frigoOuvert)
                {
                    AfficherEmplacementActuel("Frigo");

                    int count = 3;

                    EffacerContenu(3, 30);

                    foreach (KeyValuePair<Produit, int> kvp in frigo)
                    {
                        if (c == count - 3)
                        {
                            Console.BackgroundColor = ConsoleColor.White;
                            Console.ForegroundColor = ConsoleColor.Black;
                        }

                        if (count % 2 == 1)
                        {
                            Console.SetCursorPosition(2, count);
                            

                            Console.Write($"|{kvp.Key.Nom}|");

                            Console.BackgroundColor = ConsoleColor.Black;
                            Console.ForegroundColor = ConsoleColor.White;

                            Console.Write($" - Qty : {kvp.Value}");
                        }

                        else
                        {
                            Console.SetCursorPosition(35, count - 1);

                            Console.Write($"|{kvp.Key.Nom}|");

                            Console.BackgroundColor = ConsoleColor.Black;
                            Console.ForegroundColor = ConsoleColor.White;

                            Console.Write($" - Qty : {kvp.Value}");
                        }

                        count++;
                    }

                    Console.SetCursorPosition(35, 23);

                    Console.Write($"Faim : {perso.Faim} -> ");

                    if (frigo.Count > 0)
                    {
                        if (perso.Faim + frigo.ElementAt(c).Key.ImpactFaim > perso.Faim)
                        {
                            Console.ForegroundColor = ConsoleColor.DarkGreen;
                        }

                        else if (perso.Faim + frigo.ElementAt(c).Key.ImpactFaim < perso.Faim)
                        {
                            Console.ForegroundColor = ConsoleColor.DarkRed;
                        }

                        else
                        {
                            Console.ForegroundColor = ConsoleColor.White;
                        }

                        if (perso.Faim + frigo.ElementAt(c).Key.ImpactFaim >= 100)
                        {
                            Console.Write("100");
                        }

                        else if (perso.Faim + frigo.ElementAt(c).Key.ImpactFaim <= 0)
                        {
                            Console.Write("0");
                        }

                        else
                        {
                            Console.WriteLine($"{perso.Faim + frigo.ElementAt(c).Key.ImpactFaim}");
                        }
                    }


                    Console.ForegroundColor = ConsoleColor.White;


                    Console.SetCursorPosition(35, 24);

                    Console.Write($"Soif : {perso.Soif} -> ");

                    if (frigo.Count > 0)
                    {
                        if (perso.Soif + frigo.ElementAt(c).Key.ImpactSoif > perso.Soif)
                        {
                            Console.ForegroundColor = ConsoleColor.DarkGreen;
                        }

                        else if (perso.Soif + frigo.ElementAt(c).Key.ImpactSoif < perso.Soif)
                        {
                            Console.ForegroundColor = ConsoleColor.DarkRed;
                        }

                        else
                        {
                            Console.ForegroundColor = ConsoleColor.White;
                        }

                        if (perso.Soif + frigo.ElementAt(c).Key.ImpactSoif >= 100)
                        {
                            Console.Write("100");
                        }

                        else if (perso.Soif + frigo.ElementAt(c).Key.ImpactSoif <= 0)
                        {
                            Console.Write("0");
                        }

                        else
                        {
                            Console.WriteLine($"{perso.Soif + frigo.ElementAt(c).Key.ImpactSoif}");
                        }
                    }

                    Console.ForegroundColor = ConsoleColor.White;


                    Console.SetCursorPosition(35, 25);

                    Console.Write($"Énergie : {perso.Energie} -> ");

                    if (frigo.Count > 0)
                    {
                        if (perso.Energie + frigo.ElementAt(c).Key.ImpactEnergie > perso.Energie)
                        {
                            Console.ForegroundColor = ConsoleColor.DarkGreen;
                        }

                        else if (perso.Energie + frigo.ElementAt(c).Key.ImpactEnergie < perso.Energie)
                        {
                            Console.ForegroundColor = ConsoleColor.DarkRed;
                        }

                        else
                        {
                            Console.ForegroundColor = ConsoleColor.White;
                        }

                        if (perso.Energie + frigo.ElementAt(c).Key.ImpactEnergie >= 100)
                        {
                            Console.Write("100");
                        }

                        else if (perso.Energie + frigo.ElementAt(c).Key.ImpactEnergie <= 0)
                        {
                            Console.Write("0");
                        }

                        else
                        {
                            Console.WriteLine($"{perso.Energie + frigo.ElementAt(c).Key.ImpactEnergie}");
                        }
                    }

                    Console.ForegroundColor = ConsoleColor.White;


                    Console.SetCursorPosition(35, 26);

                    Console.Write($"Bonheur : {perso.Bonheur} -> ");

                    if (frigo.Count > 0)
                    {
                        if (perso.Bonheur + frigo.ElementAt(c).Key.ImpactBonheur > perso.Bonheur)
                        {
                            Console.ForegroundColor = ConsoleColor.DarkGreen;
                        }

                        else if (perso.Bonheur + frigo.ElementAt(c).Key.ImpactBonheur < perso.Bonheur)
                        {
                            Console.ForegroundColor = ConsoleColor.DarkRed;
                        }

                        else
                        {
                            Console.ForegroundColor = ConsoleColor.White;
                        }

                        if (perso.Bonheur + frigo.ElementAt(c).Key.ImpactBonheur >= 100)
                        {
                            Console.Write("100");
                        }

                        else if (perso.Bonheur + frigo.ElementAt(c).Key.ImpactBonheur <= 0)
                        {
                            Console.Write("0");
                        }

                        else
                        {
                            Console.WriteLine($"{perso.Bonheur + frigo.ElementAt(c).Key.ImpactBonheur}");
                        }
                    }

                    Console.ForegroundColor = ConsoleColor.White;

                    Console.SetCursorPosition(12, 28);

                    Console.Write($"APPUYEZ SUR [ENTER] POUR CONSOMMER");

                    Console.SetCursorPosition(12, 29);

                    Console.Write($"APPUYEZ SUR [ESCAPE] POUR QUITTER ");

                    touch = Console.ReadKey(true);

                    int longueurPanier = 0;
                    int longueurPanier2 = 0;

                    if ((int)touch.Key == 27)
                    {
                        frigoOuvert = false;
                    }

                    else if ((int)touch.Key == 13)
                    {
                        longueurPanier = frigo.Count;

                        if (frigo.Count > 0)
                        {
                            perso.Faim += frigo.ElementAt(c).Key.ImpactFaim;

                            if (perso.Faim > 100)
                            {
                                perso.Faim = 100;
                            }

                            else if (perso.Faim < 0)
                            {
                                perso.Faim = 0;
                            }

                            perso.Soif += frigo.ElementAt(c).Key.ImpactSoif;

                            if (perso.Soif > 100)
                            {
                                perso.Soif = 100;
                            }

                            else if (perso.Soif < 0)
                            {
                                perso.Soif = 0;
                            }

                            perso.Energie += frigo.ElementAt(c).Key.ImpactEnergie;

                            if (perso.Energie > 100)
                            {
                                perso.Energie = 100;
                            }

                            else if (perso.Energie < 0)
                            {
                                perso.Energie = 0;
                            }

                            perso.Bonheur += frigo.ElementAt(c).Key.ImpactBonheur;

                            if (perso.Bonheur > 100)
                            {
                                perso.Bonheur = 100;
                            }

                            else if (perso.Bonheur < 0)
                            {
                                perso.Bonheur = 0;
                            }


                            frigo[frigo.ElementAt(c).Key] -= 1;

                            if (frigo[frigo.ElementAt(c).Key] == 0)
                            {
                                frigo.Remove(frigo.ElementAt(c).Key);
                            }
                        }

                        longueurPanier2 = frigo.Count;

                        if (longueurPanier != longueurPanier2)
                        {
                            if (c > 0)
                            {
                                c--;
                            }
                        }

                        EffacerContenu(0, 30);
                    }

                    else if ((int)touch.Key == 37)
                    {
                        if (c > 0)
                        {
                            c--;
                        }
                    }

                    else if ((int)touch.Key == 38)
                    {
                        if (c - 2 >= 0)
                        {
                            c -= 2;
                        }
                    }

                    else if ((int)touch.Key == 39)
                    {
                        if (c < frigo.Count - 1)
                        {
                            c++;
                        }
                    }

                    else if ((int)touch.Key == 40)
                    {
                        if (c + 2 <= frigo.Count - 1)
                        {
                            c += 2;
                        }
                    }
                }
                EffacerContenu(0, 30);
            }

            string AjustageHeure(int _Temps)
            {
                int _Heure = _Temps / 60;
                int _Minutes = _Temps % 60;
                string _zero = "";
                string _zero2 = "";

                if (_Heure < 10)
                {
                    _zero = "0";
                }

                else
                {
                    _zero = "";
                }

                if (_Minutes < 10)
                {
                    _zero2 = "0";
                }

                else
                {
                    _zero2 = "";
                }

                return $"{_zero}{_Heure}:{_zero2}{_Minutes}";
            }

            #region Initialisation de la fenêtre
            Console.SetWindowSize(62, 32);
            Console.Title = "Simulation Of Life";
            Console.SetCursorPosition(0, 0);
            Console.CursorVisible = false; 
            #endregion

            while (Jeu)
            {
                //EcranDeJeu();
                // ÉCRAN DU MENU
                while (EcranActuel == "Menu")
                {
                    Console.BackgroundColor = ConsoleColor.White;
                    Console.ForegroundColor = ConsoleColor.Black;
                    Console.SetCursorPosition(16, 5);
                    Console.WriteLine("                            ");
                    Console.SetCursorPosition(16, 6);
                    Console.WriteLine("     Simulation Of Life     ");
                    Console.SetCursorPosition(16, 7);
                    Console.WriteLine("                            ");
                    Console.BackgroundColor = ConsoleColor.Black;
                    Console.ForegroundColor = ConsoleColor.White;

                    CaseMenu(choixOption, 0);

                    Console.SetCursorPosition(22, 12);
                    Console.WriteLine("     JOUER      ");

                    CaseMenu(choixOption, 1);

                    Console.SetCursorPosition(22, 15);
                    Console.WriteLine("  INSTRUCTIONS  ");

                    CaseMenu(choixOption, 2);

                    Console.SetCursorPosition(22, 18);
                    Console.WriteLine("    QUITTER     ");

                    touche = Console.ReadKey(true);

                    if ((int)touche.Key == 38)
                    {
                        choixOption--;

                        if (choixOption < 0)
                        {
                            choixOption = 0;
                        }
                    }

                    else if ((int)touche.Key == 40)
                    {
                        choixOption++;

                        if (choixOption > 2)
                        {
                            choixOption = 2;
                        }
                    }

                    else if ((int)touche.Key == 13)
                    {
                        if (choixOption == 0)
                        {
                            EcranActuel = "Jeu";

                        }

                        if (choixOption == 1)
                        {
                            EcranActuel = "Instructions";
                        }

                        if (choixOption == 2)
                        {
                            Jeu = false;
                            EcranActuel = "Quitter";
                        }
                        choixOption = 0;
                        Console.Clear();
                    }
                }

                // ÉCRAN DES INSTRUCTIONS
                while (EcranActuel == "Instructions")
                {
                    //EcranDeJeu();
                    Console.BackgroundColor = ConsoleColor.White;
                    Console.ForegroundColor = ConsoleColor.Black;
                    Console.SetCursorPosition(16, 5);
                    Console.WriteLine("                            ");
                    Console.SetCursorPosition(16, 6);
                    Console.WriteLine("     Simulation Of Life     ");
                    Console.SetCursorPosition(16, 7);
                    Console.WriteLine("                            ");
                    Console.BackgroundColor = ConsoleColor.Black;
                    Console.ForegroundColor = ConsoleColor.White;

                    int ligne = 12;

                    foreach (string item in listeInstructions)
                    {
                        Console.SetCursorPosition((60 - item.Length) / 2, ligne);
                        Console.WriteLine(item);
                        ligne++;
                        System.Threading.Thread.Sleep(50);
                    }

                    Console.BackgroundColor = ConsoleColor.White;
                    Console.ForegroundColor = ConsoleColor.Black;

                    Console.SetCursorPosition(22, 26);
                    Console.WriteLine("     RETOUR     ");

                    Console.BackgroundColor = ConsoleColor.Black;
                    Console.ForegroundColor = ConsoleColor.White;

                    while (EcranActuel == "Instructions")
                    {
                        touche = Console.ReadKey(true);

                        if ((int)touche.Key == 13)
                        {
                            EcranActuel = "Menu";
                            Console.Clear();
                        }
                    }
                    
                }

                // ÉCRAN DE JEU
                while (EcranActuel == "Jeu")
                {
                    // CRÉATION DU PERSONNAGE
                    if (creationPersonnage)
                    {
                        //EcranDeJeu();
                        Console.CursorVisible = true;
                        Console.BackgroundColor = ConsoleColor.White;
                        Console.ForegroundColor = ConsoleColor.Black;
                        Console.SetCursorPosition(16, 5);
                        Console.WriteLine("                            ");
                        Console.SetCursorPosition(16, 6);
                        Console.WriteLine("     Simulation Of Life     ");
                        Console.SetCursorPosition(16, 7);
                        Console.WriteLine("                            ");
                        Console.BackgroundColor = ConsoleColor.Black;
                        Console.ForegroundColor = ConsoleColor.White;

                        Console.SetCursorPosition(12, 12);
                        Console.Write("Veuillez choisir un prénom et un nom");
                        Console.SetCursorPosition(17, 13);
                        Console.Write("pour votre personnage ");

                        Console.SetCursorPosition(22, 16);
                        Console.Write("Prénom : ");
                        personnage.Prenom = Console.ReadLine();

                        Console.SetCursorPosition(25, 18);
                        Console.Write("Nom : ");
                        personnage.Nom = Console.ReadLine();
                        
                        Console.CursorVisible = false;
                        EcranActuel = "ChoixDuMetier";
                        Console.Clear();

                        //EcranDeJeu();

                        while (EcranActuel == "ChoixDuMetier")
                        {
                            Console.BackgroundColor = ConsoleColor.White;
                            Console.ForegroundColor = ConsoleColor.Black;
                            Console.SetCursorPosition(16, 5);
                            Console.WriteLine("                            ");
                            Console.SetCursorPosition(16, 6);
                            Console.WriteLine("     Simulation Of Life     ");
                            Console.SetCursorPosition(16, 7);
                            Console.WriteLine("                            ");
                            Console.BackgroundColor = ConsoleColor.Black;
                            Console.ForegroundColor = ConsoleColor.White;


                            Console.SetCursorPosition(11, 9);
                            Console.Write("Veuillez maintenant choisir un métier");

                            #region Métier = Développeur
                            if (choixOption == 0)
                            {
                                Console.SetCursorPosition(23, 20);
                                Console.WriteLine("Développeur : ");

                                Console.SetCursorPosition(20, 22);
                                Console.WriteLine("Salaire : 120 $/Jour");

                                Console.SetCursorPosition(20, 23);
                                Console.WriteLine("Coût en Énergie : 35");

                                Console.SetCursorPosition(18, 25);
                                Console.WriteLine("Facteur de risques : 3%");

                                Console.SetCursorPosition(2, 26);
                                Console.WriteLine("(Maux de dos, Déterioration de la vue, Mauvais sommeil)");

                                Console.SetCursorPosition(12, 28);
                                Console.WriteLine("[ENTER] pour sélectionner ce métier");

                                Console.BackgroundColor = ConsoleColor.White;
                                Console.ForegroundColor = ConsoleColor.Black;
                            }

                            else
                            {
                                Console.BackgroundColor = ConsoleColor.Black;
                                Console.ForegroundColor = ConsoleColor.White;
                            }

                            Console.SetCursorPosition(3, 12);
                            Console.Write(" Développeur  ");
                            #endregion

                            #region Métier = Électricien
                            if (choixOption == 1)
                            {
                                Console.SetCursorPosition(23, 20);
                                Console.WriteLine("Électricien : ");

                                Console.SetCursorPosition(20, 22);
                                Console.WriteLine("Salaire : 155 $/Jour");

                                Console.SetCursorPosition(20, 23);
                                Console.WriteLine("Coût en Énergie : 40");

                                Console.SetCursorPosition(18, 25);
                                Console.WriteLine("Facteur de risques : 5%");

                                Console.SetCursorPosition(5, 26);
                                Console.WriteLine("(Électrisation, Électrocution, Blessures diverses)");

                                Console.SetCursorPosition(12, 28);
                                Console.WriteLine("[ENTER] pour sélectionner ce métier");

                                Console.BackgroundColor = ConsoleColor.White;
                                Console.ForegroundColor = ConsoleColor.Black;
                            }

                            else
                            {
                                Console.BackgroundColor = ConsoleColor.Black;
                                Console.ForegroundColor = ConsoleColor.White;
                            }

                            Console.SetCursorPosition(22, 12);
                            Console.Write(" Électricien  ");
                            #endregion

                            #region Métier = Médecin
                            if (choixOption == 2)
                            {
                                Console.SetCursorPosition(25, 20);
                                Console.WriteLine("Médecin : ");

                                Console.SetCursorPosition(20, 22);
                                Console.WriteLine("Salaire : 225 $/Jour");

                                Console.SetCursorPosition(20, 23);
                                Console.WriteLine("Coût en Énergie : 60");

                                Console.SetCursorPosition(18, 25);
                                Console.WriteLine("Facteur de risques : 6%");

                                Console.SetCursorPosition(10, 26);
                                Console.WriteLine("(Stress important, Contraction de virus)");

                                Console.SetCursorPosition(12, 28);
                                Console.WriteLine("[ENTER] pour sélectionner ce métier");

                                Console.BackgroundColor = ConsoleColor.White;
                                Console.ForegroundColor = ConsoleColor.Black;
                            }

                            else
                            {
                                Console.BackgroundColor = ConsoleColor.Black;
                                Console.ForegroundColor = ConsoleColor.White;
                            }

                            Console.SetCursorPosition(41, 12);
                            Console.Write("   Médecin    ");
                            #endregion

                            #region Métier = Plombier
                            if (choixOption == 3)
                            {
                                Console.SetCursorPosition(24, 20);
                                Console.WriteLine("Plombier : ");

                                Console.SetCursorPosition(20, 22);
                                Console.WriteLine("Salaire : 145 $/Jour");

                                Console.SetCursorPosition(20, 23);
                                Console.WriteLine("Coût en Énergie : 47");

                                Console.SetCursorPosition(18, 25);
                                Console.WriteLine("Facteur de risques : 2%");

                                Console.SetCursorPosition(15, 26);
                                Console.WriteLine("(Blessures diverses, Brûlures)");

                                Console.SetCursorPosition(12, 28);
                                Console.WriteLine("[ENTER] pour sélectionner ce métier");

                                Console.BackgroundColor = ConsoleColor.White;
                                Console.ForegroundColor = ConsoleColor.Black;
                            }

                            else
                            {
                                Console.BackgroundColor = ConsoleColor.Black;
                                Console.ForegroundColor = ConsoleColor.White;
                            }

                            Console.SetCursorPosition(3, 14);
                            Console.Write("   Plombier   ");
                            #endregion

                            #region Métier = Acteur
                            if (choixOption == 4)
                            {
                                Console.SetCursorPosition(25, 20);
                                Console.WriteLine("Acteur : ");

                                Console.SetCursorPosition(20, 22);
                                Console.WriteLine("Salaire : 185 $/Jour");

                                Console.SetCursorPosition(20, 23);
                                Console.WriteLine("Coût en Énergie : 52");

                                Console.SetCursorPosition(18, 25);
                                Console.WriteLine("Facteur de risques : 4%");

                                Console.SetCursorPosition(5, 26);
                                Console.WriteLine("(Accident de tournage, Cascade manquée, Blessures)");

                                Console.SetCursorPosition(12, 28);
                                Console.WriteLine("[ENTER] pour sélectionner ce métier");

                                Console.BackgroundColor = ConsoleColor.White;
                                Console.ForegroundColor = ConsoleColor.Black;
                            }

                            else
                            {
                                Console.BackgroundColor = ConsoleColor.Black;
                                Console.ForegroundColor = ConsoleColor.White;
                            }

                            Console.SetCursorPosition(22, 14);
                            Console.Write("    Acteur    ");
                            #endregion

                            #region Métier = Policier
                            if (choixOption == 5)
                            {
                                Console.SetCursorPosition(24, 20);
                                Console.WriteLine("Policier : ");

                                Console.SetCursorPosition(20, 22);
                                Console.WriteLine("Salaire : 200 $/Jour");

                                Console.SetCursorPosition(20, 23);
                                Console.WriteLine("Coût en Énergie : 60");

                                Console.SetCursorPosition(18, 25);
                                Console.WriteLine("Facteur de risques : 5%");

                                Console.SetCursorPosition(13, 26);
                                Console.WriteLine("(Balle perdue, Blessures diverses)");

                                Console.SetCursorPosition(12, 28);
                                Console.WriteLine("[ENTER] pour sélectionner ce métier");

                                Console.BackgroundColor = ConsoleColor.White;
                                Console.ForegroundColor = ConsoleColor.Black;
                            }

                            else
                            {
                                Console.BackgroundColor = ConsoleColor.Black;
                                Console.ForegroundColor = ConsoleColor.White;
                            }

                            Console.SetCursorPosition(41, 14);
                            Console.Write("   Policier   ");
                            #endregion

                            #region Métier = Enseignant
                            if (choixOption == 6)
                            {
                                Console.SetCursorPosition(23, 20);
                                Console.WriteLine("Enseignant : ");

                                Console.SetCursorPosition(20, 22);
                                Console.WriteLine("Salaire : 130 $/Jour");

                                Console.SetCursorPosition(20, 23);
                                Console.WriteLine("Coût en Énergie : 40");

                                Console.SetCursorPosition(18, 25);
                                Console.WriteLine("Facteur de risques : 3%");

                                Console.SetCursorPosition(15, 26);
                                Console.WriteLine("(Stress important, Dépression)");

                                Console.SetCursorPosition(12, 28);
                                Console.WriteLine("[ENTER] pour sélectionner ce métier");

                                Console.BackgroundColor = ConsoleColor.White;
                                Console.ForegroundColor = ConsoleColor.Black;
                            }

                            else
                            {
                                Console.BackgroundColor = ConsoleColor.Black;
                                Console.ForegroundColor = ConsoleColor.White;
                            }

                            Console.SetCursorPosition(3, 16);
                            Console.Write("  Enseignant  ");
                            #endregion

                            #region Métier = Cuisinier
                            if (choixOption == 7)
                            {
                                Console.SetCursorPosition(24, 20);
                                Console.WriteLine("Cuisinier : ");

                                Console.SetCursorPosition(20, 22);
                                Console.WriteLine("Salaire : 160 $/Jour");

                                Console.SetCursorPosition(20, 23);
                                Console.WriteLine("Coût en Énergie : 50");

                                Console.SetCursorPosition(18, 25);
                                Console.WriteLine("Facteur de risques : 6%");

                                Console.SetCursorPosition(20, 26);
                                Console.WriteLine("(Brûlures, Coupures)");

                                Console.SetCursorPosition(12, 28);
                                Console.WriteLine("[ENTER] pour sélectionner ce métier");

                                Console.BackgroundColor = ConsoleColor.White;
                                Console.ForegroundColor = ConsoleColor.Black;
                            }

                            else
                            {
                                Console.BackgroundColor = ConsoleColor.Black;
                                Console.ForegroundColor = ConsoleColor.White;
                            }

                            Console.SetCursorPosition(22, 16);
                            Console.Write("  Cuisinier   ");
                            #endregion

                            #region Métier = Mécanicien
                            if (choixOption == 8)
                            {
                                Console.SetCursorPosition(23, 20);
                                Console.WriteLine("Mécanicien : ");

                                Console.SetCursorPosition(20, 22);
                                Console.WriteLine("Salaire : 210 $/Jour");

                                Console.SetCursorPosition(20, 23);
                                Console.WriteLine("Coût en Énergie : 44");

                                Console.SetCursorPosition(18, 25);
                                Console.WriteLine("Facteur de risques : 5%");

                                Console.SetCursorPosition(14, 26);
                                Console.WriteLine("(Brûlures, Accident, Blessures)");

                                Console.SetCursorPosition(12, 28);
                                Console.WriteLine("[ENTER] pour sélectionner ce métier");

                                Console.BackgroundColor = ConsoleColor.White;
                                Console.ForegroundColor = ConsoleColor.Black;
                            }

                            else
                            {
                                Console.BackgroundColor = ConsoleColor.Black;
                                Console.ForegroundColor = ConsoleColor.White;
                            }

                            Console.SetCursorPosition(41, 16);
                            Console.Write("  Mécanicien  "); 
                            #endregion

                            Console.BackgroundColor = ConsoleColor.Black;
                            Console.ForegroundColor = ConsoleColor.White;

                            //Détection de la touche appuyée
                            touche = Console.ReadKey(true);

                            //Appui sur la flèche de gauche
                            if ((int)touche.Key == 37)
                            {
                                if ((choixOption - 1) >= 0)
                                {
                                    choixOption--;
                                }
                            }

                            //Appui sur la flèche du haut
                            else if ((int)touche.Key == 38)
                            {
                                if ((choixOption - 3) >= 0)
                                {
                                    choixOption -= 3;
                                }
                            }

                            //Appui sur la flèche de droite
                            else if ((int)touche.Key == 39)
                            {
                                if ((choixOption + 1) <= 8)
                                {
                                    choixOption++;
                                }
                            }

                            //Appui sur la flèche du bas
                            else if ((int)touche.Key == 40)
                            {
                                if ((choixOption + 3) <= 8)
                                {
                                    choixOption += 3;
                                }
                            }

                            //Appui sur Enter
                            else if ((int)touche.Key == 13)
                            {
                                if (choixOption == 0)
                                {
                                    metier.NomDuMetier = "Développeur";
                                    metier.SalaireHoraire = 120;
                                    metier.CoutEnEnergie = 35;
                                    metier.RisqueDuMetier = 3;
                                    metier.HeureDeDebut = 480;
                                    metier.HeureDeFin = metier.HeureDeDebut + 480;
                                }

                                else if (choixOption == 1)
                                {
                                    metier.NomDuMetier = "Électricien";
                                    metier.SalaireHoraire = 155;
                                    metier.CoutEnEnergie = 40;
                                    metier.RisqueDuMetier = 5;
                                    metier.HeureDeDebut = 465;
                                    metier.HeureDeFin = metier.HeureDeDebut + 510;
                                }

                                else if (choixOption == 2)
                                {
                                    metier.NomDuMetier = "Médecin";
                                    metier.SalaireHoraire = 225;
                                    metier.CoutEnEnergie = 60;
                                    metier.RisqueDuMetier = 6;
                                    metier.HeureDeDebut = 450;
                                    metier.HeureDeFin = metier.HeureDeDebut + 600;
                                }

                                else if (choixOption == 3)
                                {
                                    metier.NomDuMetier = "Plombier";
                                    metier.SalaireHoraire = 145;
                                    metier.CoutEnEnergie = 47;
                                    metier.RisqueDuMetier = 2;
                                    metier.HeureDeDebut = 465;
                                    metier.HeureDeFin = metier.HeureDeDebut + 510;
                                }

                                else if (choixOption == 4)
                                {
                                    metier.NomDuMetier = "Acteur";
                                    metier.SalaireHoraire = 185;
                                    metier.CoutEnEnergie = 52;
                                    metier.RisqueDuMetier = 4;
                                    metier.HeureDeDebut = 540;
                                    metier.HeureDeFin = metier.HeureDeDebut + 540;
                                }

                                else if (choixOption == 5)
                                {
                                    metier.NomDuMetier = "Policier";
                                    metier.SalaireHoraire = 200;
                                    metier.CoutEnEnergie = 60;
                                    metier.RisqueDuMetier = 5;
                                    metier.HeureDeDebut = 1320;
                                    metier.HeureDeFin = 360;
                                }

                                else if (choixOption == 6)
                                {
                                    metier.NomDuMetier = "Enseignant";
                                    metier.SalaireHoraire = 130;
                                    metier.CoutEnEnergie = 40;
                                    metier.RisqueDuMetier = 3;
                                    metier.HeureDeDebut = 510;
                                    metier.HeureDeFin = metier.HeureDeDebut + 480;
                                }

                                else if (choixOption == 7)
                                {
                                    metier.NomDuMetier = "Cuisinier";
                                    metier.SalaireHoraire = 160;
                                    metier.CoutEnEnergie = 50;
                                    metier.RisqueDuMetier = 6;
                                    metier.HeureDeDebut = 840;
                                    metier.HeureDeFin = metier.HeureDeDebut + 510;
                                }

                                else if (choixOption == 8)
                                {
                                    metier.NomDuMetier = "Mécanicien";
                                    metier.SalaireHoraire = 210;
                                    metier.CoutEnEnergie = 44;
                                    metier.RisqueDuMetier = 5;
                                    metier.HeureDeDebut = 480;
                                    metier.HeureDeFin = metier.HeureDeDebut + 510;
                                }

                                EcranActuel = "Jeu";
                            }

                            for (int i = 20; i < 30; i++)
                            {
                                Console.SetCursorPosition(0, i);
                                Console.WriteLine("                                                            ");
                            }
                        }

                        Console.Clear();
                        //EcranDeJeu();
                        creationPersonnage = false;
                        choixOption = 0;
                    }

                    // ANIMATION D'INTRODUCTION
                    if (animationIntro)
                    {
                        System.Threading.Thread.Sleep(1500);

                        if (personnage.Prenom.Length % 2 == 1)
                        {
                            Console.SetCursorPosition(((60 - personnage.Prenom.Length + 1) / 2) - 3, 15);
                        }

                        else
                        {
                            Console.SetCursorPosition(((60 - personnage.Prenom.Length) / 2) - 3, 15);
                        }

                        Console.WriteLine($"{personnage.Prenom} ... ?");

                        System.Threading.Thread.Sleep(1500);

                        Console.Clear();
                        //EcranDeJeu();

                        System.Threading.Thread.Sleep(3000);
                        Console.SetCursorPosition(8, 15);
                        Console.WriteLine("Ta mère et moi avons longuement discuté ...");
                        System.Threading.Thread.Sleep(2000);
                        Console.SetCursorPosition(12, 17);
                        Console.WriteLine("Et nous pensons qu'il est temps que");
                        Console.SetCursorPosition(13, 18);
                        Console.WriteLine("tu battes de tes propres ailes ...");
                        System.Threading.Thread.Sleep(4000);

                        Console.Clear();
                        //EcranDeJeu();
                        System.Threading.Thread.Sleep(2000);

                        Console.SetCursorPosition(26, 15);
                        Console.Write("Ainsi, ");
                        System.Threading.Thread.Sleep(1500);
                        Console.SetCursorPosition(16, 16);
                        Console.Write("pour que ton départ se fasse");
                        Console.SetCursorPosition(17, 17);
                        Console.Write("au mieux et au plus vite,");
                        System.Threading.Thread.Sleep(2200);
                        Console.SetCursorPosition(6, 18);
                        Console.Write("nous t'avons trouvé ce super appart bon marché !");
                        System.Threading.Thread.Sleep(3500);

                        Console.Clear();
                        //EcranDeJeu();
                        System.Threading.Thread.Sleep(1000);

                        Console.SetCursorPosition(16, 15);
                        Console.Write("Voilà un peu d'argent de poche.");
                        System.Threading.Thread.Sleep(1500);
                        Console.SetCursorPosition(16, 16);
                        Console.Write("Maintenant bon vent et surtout,");
                        System.Threading.Thread.Sleep(1000);
                        Console.SetCursorPosition(23, 17);
                        Console.Write("Bon Courage !");
                        System.Threading.Thread.Sleep(3000);

                        Console.Clear();
                        //EcranDeJeu();
                        System.Threading.Thread.Sleep(2000);

                        animationIntro = false;
                    }

                    stopwatch = System.Diagnostics.Stopwatch.StartNew();
                    dir = 1;

                    // JEU EN COURS
                    while (Jeu)
                    {
                        #region Affichage de la localisation
                        AfficherEmplacementActuel("Domicile");
                        #endregion

                        #region Affichage des barres d'état
                        Console.SetCursorPosition(1, 3);
                        Console.Write($" SANTÉ  : {personnage.Sante} / 100 ");
                        Console.SetCursorPosition(22, 3);

                        if (personnage.Sante >= 70)
                        {
                            Console.BackgroundColor = ConsoleColor.Green;
                        }

                        else if (personnage.Sante >= 40)
                        {
                            Console.BackgroundColor = ConsoleColor.DarkYellow;
                        }

                        else
                        {
                            Console.BackgroundColor = ConsoleColor.DarkRed;
                        }

                        for (int i = 0; i < (personnage.Sante / 5) + 1; i++)
                        {
                            Console.Write(" ");
                        }

                        Console.BackgroundColor = ConsoleColor.Black;
                        Console.SetCursorPosition(1, 5);
                        Console.Write($"ÉNERGIE : {personnage.Energie} / 100 ");
                        Console.SetCursorPosition(22, 5);

                        if (personnage.Energie >= 70)
                        {
                            Console.BackgroundColor = ConsoleColor.Green;
                        }

                        else if (personnage.Energie >= 40)
                        {
                            Console.BackgroundColor = ConsoleColor.DarkYellow;
                        }

                        else
                        {
                            Console.BackgroundColor = ConsoleColor.DarkRed;
                        }

                        for (int i = 0; i < (personnage.Energie / 5) + 1; i++)
                        {
                            Console.Write(" ");
                        }

                        Console.BackgroundColor = ConsoleColor.Black;
                        Console.SetCursorPosition(1, 7);
                        Console.Write($" FAIM   : {personnage.Faim} / 100 ");
                        Console.SetCursorPosition(22, 7);

                        if (personnage.Faim >= 70)
                        {
                            Console.BackgroundColor = ConsoleColor.Green;
                        }

                        else if (personnage.Faim >= 40)
                        {
                            Console.BackgroundColor = ConsoleColor.DarkYellow;
                        }

                        else
                        {
                            Console.BackgroundColor = ConsoleColor.DarkRed;
                        }

                        for (int i = 0; i < (personnage.Faim / 5) + 1; i++)
                        {
                            Console.Write(" ");
                        }

                        Console.BackgroundColor = ConsoleColor.Black;
                        Console.SetCursorPosition(1, 9);
                        Console.Write($" SOIF   : {personnage.Soif} / 100 ");
                        Console.SetCursorPosition(22, 9);

                        if (personnage.Soif >= 70)
                        {
                            Console.BackgroundColor = ConsoleColor.Green;
                        }

                        else if (personnage.Soif >= 40)
                        {
                            Console.BackgroundColor = ConsoleColor.DarkYellow;
                        }

                        else
                        {
                            Console.BackgroundColor = ConsoleColor.DarkRed;
                        }

                        for (int i = 0; i < (personnage.Soif / 5) + 1; i++)
                        {
                            Console.Write(" ");
                        }
                        Console.BackgroundColor = ConsoleColor.Black;
                        
                        Console.SetCursorPosition(1, 11);
                        Console.Write($"HYGIENE : {personnage.Hygiene} / 100 ");
                        Console.SetCursorPosition(22, 11);

                        if (personnage.Hygiene >= 70)
                        {
                            Console.BackgroundColor = ConsoleColor.Green;
                        }

                        else if (personnage.Hygiene >= 40)
                        {
                            Console.BackgroundColor = ConsoleColor.DarkYellow;
                        }

                        else
                        {
                            Console.BackgroundColor = ConsoleColor.DarkRed;
                        }

                        for (int i = 0; i < (personnage.Hygiene / 5) + 1; i++)
                        {
                            Console.Write(" ");
                        }
                        Console.BackgroundColor = ConsoleColor.Black;

                        Console.SetCursorPosition(1, 13);
                        Console.Write($"BONHEUR : {personnage.Bonheur} / 100 ");
                        Console.SetCursorPosition(22, 13);

                        if (personnage.Bonheur >= 70)
                        {
                            Console.BackgroundColor = ConsoleColor.Green;
                        }

                        else if (personnage.Bonheur >= 40)
                        {
                            Console.BackgroundColor = ConsoleColor.DarkYellow;
                        }

                        else
                        {
                            Console.BackgroundColor = ConsoleColor.DarkRed;
                        }

                        for (int i = 0; i < (personnage.Bonheur / 5) + 1; i++)
                        {
                            Console.Write(" ");
                        }
                        Console.BackgroundColor = ConsoleColor.Black;
                        #endregion

                        #region Affichage de la fiche signalétique du joueur
                        if (("NOM : " + personnage.Nom).Length % 2 == 1)
                        {
                            Console.SetCursorPosition((20 - (("NOM : " + personnage.Nom).Length + 1)) / 2, 15);
                        }

                        else
                        {
                            Console.SetCursorPosition((20 - (("NOM : " + personnage.Nom).Length)) / 2, 15);
                        }

                        Console.Write("NOM : " + personnage.Nom);

                        if (("PRÉNOM : " + personnage.Prenom).Length % 2 == 1)
                        {
                            Console.SetCursorPosition(((20 - (("PRÉNOM : " + personnage.Prenom).Length + 1)) / 2) + 20, 15);
                        }

                        else
                        {
                            Console.SetCursorPosition(((20 - (("PRÉNOM : " + personnage.Prenom).Length)) / 2) + 20, 15);
                        }

                        Console.Write("PRÉNOM : " + personnage.Prenom);

                        if (("AGE : " + personnage.Age).Length % 2 == 1)
                        {
                            Console.SetCursorPosition(((20 - (("AGE : " + personnage.Age).Length + 1) / 2)) + 40, 15);
                        }

                        else
                        {
                            Console.SetCursorPosition(((20 - (("AGE : " + personnage.Age).Length)) / 2) + 40, 15);
                        }

                        Console.Write("AGE : " + personnage.Age);

                        if (("MÉTIER : " + metier.NomDuMetier).Length % 2 == 1)
                        {
                            Console.SetCursorPosition((30 - (("MÉTIER : " + metier.NomDuMetier).Length + 1)) / 2, 17);
                        }

                        else
                        {
                            Console.SetCursorPosition((30 - (("MÉTIER : " + metier.NomDuMetier).Length)) / 2, 17);
                        }

                        Console.Write("MÉTIER : " + metier.NomDuMetier);

                        if (($"SALAIRE : {metier.SalaireHoraire} $ / JOUR").Length % 2 == 1)
                        {
                            Console.SetCursorPosition(((30 - ($"SALAIRE : {metier.SalaireHoraire} $ / JOUR").Length + 1) / 2) + 30, 17);
                        }

                        else
                        {
                            Console.SetCursorPosition(((30 - ($"SALAIRE : {metier.SalaireHoraire} $ / JOUR").Length) / 2) + 30, 17);
                        }

                        Console.Write($"SALAIRE : {metier.SalaireHoraire} $ / JOUR");
                        #endregion

                        #region Affichage du portefeuille et du temps
                        if (($"ARGENT : {personnage.Argent} $").Length % 2 == 1)
                        {
                            Console.SetCursorPosition((30 - (($"ARGENT : {personnage.Argent} $").Length + 1)) / 2, 19);
                        }

                        else
                        {
                            Console.SetCursorPosition((30 - (($"ARGENT : {personnage.Argent} $").Length)) / 2, 19);
                        }

                        Console.Write($"ARGENT : {personnage.Argent} $");

                        if (Temps >= 1440)
                        {
                            Temps -= 1440;
                            JourActuel++;
                            JourDeLaSemaineActuel++;
                            compteurDeJours++;
                        }

                        if (MoisActuel == 1)
                        {
                            if (JourActuel > 28)
                            {
                                JourActuel -= 28;
                                MoisActuel++;
                            }

                            if (JourDeLaSemaineActuel > 6)
                            {
                                JourDeLaSemaineActuel -= 7;
                            }
                        }

                        else if (MoisActuel == 0 || MoisActuel == 2 || MoisActuel == 4 || MoisActuel == 6 || MoisActuel == 7 || MoisActuel == 9 || MoisActuel == 11)
                        {
                            if (JourActuel > 31)
                            {
                                JourActuel -= 31;
                                MoisActuel++;
                            }

                            if (JourDeLaSemaineActuel > 6)
                            {
                                JourDeLaSemaineActuel -= 7;
                            }
                        }

                        else
                        {
                            if (JourActuel > 30)
                            {
                                JourActuel -= 30;
                                MoisActuel++;
                            }

                            if (JourDeLaSemaineActuel > 6)
                            {
                                JourDeLaSemaineActuel -= 7;
                            }
                        }

                        if (MoisActuel > 11)
                        {
                            MoisActuel -= 12;
                        }

                        Heure = Temps / 60;
                        Minutes = Temps % 60;

                        if (Heure < 10)
                        {
                            zero = "0";
                        }

                        else
                        {
                            zero = "";
                        }

                        if (Minutes < 10)
                        {
                            zero2 = "0";
                        }

                        else
                        {
                            zero2 = "";
                        }

                        if (JourActuel < 10)
                        {
                            zero3 = "0";
                        }

                        else
                        {
                            zero3 = "";
                        }

                        if (($"{zero}{Heure}:{zero2}{Minutes} {JourDeLaSemaine[JourDeLaSemaineActuel]} {zero3}{JourActuel} {Mois[MoisActuel]}").Length % 2 == 1)
                        {
                            Console.SetCursorPosition(((30 - ($"{zero}{Heure}:{zero2}{Minutes} {JourDeLaSemaine[JourDeLaSemaineActuel]} {zero3}{JourActuel} {Mois[MoisActuel]}").Length + 1) / 2) + 30, 19);
                        }

                        else
                        {
                            Console.SetCursorPosition(((30 - ($"{zero}{Heure}:{zero2}{Minutes} {JourDeLaSemaine[JourDeLaSemaineActuel]} {zero3}{JourActuel} {Mois[MoisActuel]}").Length) / 2) + 30, 19);
                        }

                        Console.Write($"{zero}{Heure}:{zero2}{Minutes} {JourDeLaSemaine[JourDeLaSemaineActuel]} {zero3}{JourActuel} {Mois[MoisActuel]}");
                        #endregion

                        #region Curseur sur l'action 'Shopping'
                        if (choixOption == 0)
                        {
                            if (dir != 0)
                            {
                                Console.BackgroundColor = ConsoleColor.Black;
                                Console.ForegroundColor = ConsoleColor.White;

                                for (int i = 21; i < 30; i++)
                                {
                                    Console.SetCursorPosition(21, i);
                                    Console.Write("                                      ");
                                }

                                Console.SetCursorPosition(21, 21);

                                Console.Write("         ALLEZ AU SUPERMARCHÉ         ");

                                Console.SetCursorPosition(21, 22);

                                Console.Write("       POUR REMPLIR VOTRE FRIGO       ");

                                Console.SetCursorPosition(21, 24);

                                Console.Write("  LE SUPERMARCHÉ EST OUVERT TOUT LES  ");

                                Console.SetCursorPosition(21, 25);

                                Console.Write("        JOURS DE 08:00 A 20:00        ");

                                if (Temps >= 480 && Temps < 1200)
                                {
                                    Console.SetCursorPosition(36, 26);

                                    Console.BackgroundColor = ConsoleColor.DarkGreen;
                                    Console.ForegroundColor = ConsoleColor.Black;

                                    Console.Write("  OUVERT  ");
                                }

                                else
                                {
                                    Console.SetCursorPosition(36, 26);

                                    Console.BackgroundColor = ConsoleColor.DarkRed;
                                    Console.ForegroundColor = ConsoleColor.Black;

                                    Console.Write("  FERMÉ  ");
                                }

                                Console.BackgroundColor = ConsoleColor.Black;
                                Console.ForegroundColor = ConsoleColor.White;

                                Console.SetCursorPosition(21, 27);

                                Console.Write($"       TEMPS DE L'ACTION : {AjustageHeure(_AllerAuMagasin.TempsDeLAction)}");

                                Console.SetCursorPosition(21, 28);

                                Console.Write($"         COÛT EN ÉNERGIE : {_AllerAuMagasin.CoutEnEnergie}         ");
                            }

                            Console.BackgroundColor = ConsoleColor.White;
                            Console.ForegroundColor = ConsoleColor.Black;
                        }

                        else
                        {
                            Console.BackgroundColor = ConsoleColor.Black;
                            Console.ForegroundColor = ConsoleColor.White;
                        }

                        Console.SetCursorPosition(1, 21);
                        Console.Write("      SHOPPING      "); 
                        #endregion

                        #region Curseur sur l'action 'Travailler'
                        if (choixOption == 1)
                        {
                            if (dir != 0)
                            {
                                Console.BackgroundColor = ConsoleColor.Black;
                                Console.ForegroundColor = ConsoleColor.White;

                                for (int i = 21; i < 30; i++)
                                {
                                    Console.SetCursorPosition(21, i);
                                    Console.Write("                                      ");
                                }

                                Console.SetCursorPosition(21, 21);

                                Console.Write("           ALLEZ AU TRAVAIL           ");

                                Console.SetCursorPosition(21, 22);

                                Console.Write("       POUR GAGNER DE L'ARGENT        ");

                                Console.SetCursorPosition(21, 23);

                                Console.Write("            EN FIN DE MOIS            ");

                                Console.SetCursorPosition(21, 24);


                                Heure = metier.HeureDeDebut / 60;
                                Minutes = metier.HeureDeDebut % 60;

                                Heure2 = metier.HeureDeFin / 60;
                                Minutes2 = metier.HeureDeFin % 60;

                                Console.Write($"      HORAIRE : {AjustageHeure(metier.HeureDeDebut)} - {AjustageHeure(metier.HeureDeFin)} DU      ");

                                Console.SetCursorPosition(21, 25);

                                Console.Write("          LUNDI AU VENDREDI           ");

                                Console.SetCursorPosition(21, 28);

                                if (metier.HeureDeDebut > metier.HeureDeFin)
                                {
                                    echelon = ((metier.HeureDeFin + 1440) - metier.HeureDeDebut + 30);
                                    Console.Write($"       TEMPS DE L'ACTION : {AjustageHeure((metier.HeureDeFin + 1440) - metier.HeureDeDebut + 30)}");
                                }

                                else
                                {
                                    echelon = (metier.HeureDeFin - metier.HeureDeDebut + 30);
                                    Console.Write($"       TEMPS DE L'ACTION : {AjustageHeure(metier.HeureDeFin - metier.HeureDeDebut + 30)}");
                                }

                                Console.SetCursorPosition(21, 26);

                                Console.Write($"      VOUS DEVEZ PARTIR ENTRE");

                                Console.SetCursorPosition(21, 27);

                                Console.Write($"           { AjustageHeure(metier.HeureDeDebut - 30)} ET { AjustageHeure(metier.HeureDeDebut + 30)}");

                                Console.SetCursorPosition(21, 29);

                                _Travailler.CoutEnEnergie = metier.CoutEnEnergie;

                                Console.Write($"         COÛT EN ÉNERGIE : {_Travailler.CoutEnEnergie}         ");
                            }

                            Console.BackgroundColor = ConsoleColor.White;
                            Console.ForegroundColor = ConsoleColor.Black;
                        }

                        else
                        {
                            Console.BackgroundColor = ConsoleColor.Black;
                            Console.ForegroundColor = ConsoleColor.White;
                        }

                        Console.SetCursorPosition(1, 22);
                        Console.Write("     TRAVAILLER     "); 
                        #endregion

                        #region Curseur sur l'action 'Se laver'
                        if (choixOption == 2)
                        {
                            if (dir != 0)
                            {
                                Console.BackgroundColor = ConsoleColor.Black;
                                Console.ForegroundColor = ConsoleColor.White;

                                for (int i = 21; i < 30; i++)
                                {
                                    Console.SetCursorPosition(21, i);
                                    Console.Write("                                      ");
                                }

                                Console.SetCursorPosition(21, 21);

                                Console.Write("       PRENDRE UNE DOUCHE POUR        ");

                                Console.SetCursorPosition(21, 22);

                                Console.Write("            VOTRE HYGIENE             ");

                                Console.SetCursorPosition(21, 24);

                                Console.Write($"       TEMPS DE L'ACTION : {AjustageHeure(_SeLaver.TempsDeLAction)}");

                                Console.SetCursorPosition(21, 25);

                                Console.Write($"         COÛT EN ÉNERGIE : {_SeLaver.CoutEnEnergie}         ");

                                Console.SetCursorPosition(21, 26);

                                Console.Write($"            BONHEUR : + {_SeLaver.RapportBonheur}            ");
                            }

                            Console.BackgroundColor = ConsoleColor.White;
                            Console.ForegroundColor = ConsoleColor.Black;
                        }

                        else
                        {
                            Console.BackgroundColor = ConsoleColor.Black;
                            Console.ForegroundColor = ConsoleColor.White;
                        }

                        Console.SetCursorPosition(1, 23);
                        Console.Write("      SE LAVER      "); 
                        #endregion

                        #region Curseur sur l'action 'Frigo'
                        if (choixOption == 3)
                        {
                            if (dir != 0)
                            {
                                Console.BackgroundColor = ConsoleColor.Black;
                                Console.ForegroundColor = ConsoleColor.White;

                                for (int i = 21; i < 30; i++)
                                {
                                    Console.SetCursorPosition(21, i);
                                    Console.Write("                                      ");
                                }

                                Console.SetCursorPosition(21, 22);

                                Console.Write("           OUVRIR LE FRIGO            ");
                            }

                            Console.BackgroundColor = ConsoleColor.White;
                            Console.ForegroundColor = ConsoleColor.Black;
                        }

                        else
                        {
                            Console.BackgroundColor = ConsoleColor.Black;
                            Console.ForegroundColor = ConsoleColor.White;
                        }

                        Console.SetCursorPosition(1, 24);
                        Console.Write("       FRIGO        "); 
                        #endregion

                        #region Curseur sur l'action 'Dormir'
                        if (choixOption == 4)
                        {
                            if (dir != 0)
                            {
                                Console.BackgroundColor = ConsoleColor.Black;
                                Console.ForegroundColor = ConsoleColor.White;

                                for (int i = 21; i < 30; i++)
                                {
                                    Console.SetCursorPosition(21, i);
                                    Console.Write("                                      ");
                                }

                                Console.SetCursorPosition(21, 22);

                                Console.Write("        DORMIR POUR RÉCUPERER         ");

                                Console.SetCursorPosition(21, 23);

                                Console.Write("             DE L'ÉNERGIE             ");

                            }

                            Console.BackgroundColor = ConsoleColor.White;
                            Console.ForegroundColor = ConsoleColor.Black;
                        }

                        else
                        {
                            Console.BackgroundColor = ConsoleColor.Black;
                            Console.ForegroundColor = ConsoleColor.White;
                        }

                        Console.SetCursorPosition(1, 25);
                        Console.Write("       DORMIR       ");

                        Console.BackgroundColor = ConsoleColor.Black;
                        Console.ForegroundColor = ConsoleColor.White;
                        #endregion


                        if (choixOption == 5)
                        {
                            if (dir != 0)
                            {
                                Console.BackgroundColor = ConsoleColor.Black;
                                Console.ForegroundColor = ConsoleColor.White;

                                for (int i = 21; i < 30; i++)
                                {
                                    Console.SetCursorPosition(21, i);
                                    Console.Write("                                      ");
                                }

                                Console.SetCursorPosition(21, 22);

                                Console.Write("          AUGMENTATION DE LA");

                                Console.SetCursorPosition(21, 23);

                                Console.Write("               VITESSE");
                            }

                            Console.BackgroundColor = ConsoleColor.White;
                            Console.ForegroundColor = ConsoleColor.Black;
                        }

                        else
                        {
                            Console.BackgroundColor = ConsoleColor.Black;
                            Console.ForegroundColor = ConsoleColor.White;
                        }

                        Console.SetCursorPosition(1, 26);
                        Console.Write("     ACCÉLÉRER    ");

                        if (vitesse == -1)
                        {
                            Console.BackgroundColor = ConsoleColor.DarkRed;
                        }

                        else if (vitesse == 1)
                        {
                            Console.BackgroundColor = ConsoleColor.DarkGreen;
                        }

                        Console.Write("  ");



                        Console.BackgroundColor = ConsoleColor.Black;
                        Console.ForegroundColor = ConsoleColor.White;

                        //Détection de la touche appuyée
                        dir = 0;
                        if (Console.KeyAvailable)
                        {
                            touche = Console.ReadKey(true);
                            dir = (int)touche.Key;
                        }
                        
                        //Appui flèche du haut
                        if (dir == 38 && choixOption > 0)
                        {
                            if (choixOption > 0)
                            {
                                choixOption--;
                            }
                        }

                        //Appui Flèche du bas
                        else if (dir == 40 && choixOption < 5)
                        {
                            if (choixOption < 5)
                            {
                                choixOption++;
                            }
                        }

                        //Appui sur ENTER
                        else if (dir == 13)
                        {
                            if (choixOption == 0 && Temps >= 480 && Temps < 1200)
                            {
                                Magasin(out personnage, out ContenuFrigo, ProduitsDisponiblesAuMagasin);
                                Temps += _AllerAuMagasin.TempsDeLAction;
                                personnage.Energie -= _AllerAuMagasin.CoutEnEnergie;
                                personnage.Hygiene -= nombreAleatoire.Next(5, 11);

                                EffacerContenu(0, 30);
                                Console.SetCursorPosition(17, 15);
                                Console.Write($"{personnage.Prenom} rentre à l'appart");
                                System.Threading.Thread.Sleep(2000);
                                EffacerContenu(0, 30);
                            }

                            else if (choixOption == 1)
                            {
                                if (Temps >= metier.HeureDeDebut - 60 && Temps <= metier.HeureDeDebut + 60)
                                {
                                    if (JourDeLaSemaine[JourDeLaSemaineActuel] == "LUN"
                                        || JourDeLaSemaine[JourDeLaSemaineActuel] == "MAR"
                                        || JourDeLaSemaine[JourDeLaSemaineActuel] == "MER"
                                        || JourDeLaSemaine[JourDeLaSemaineActuel] == "JEU"
                                        || JourDeLaSemaine[JourDeLaSemaineActuel] == "VEN")
                                    {
                                        bool travailEnCours = true;

                                        double pourcentage = 0;

                                        double progression = 0;

                                        int echelon2 = (echelon / 15);

                                        EffacerContenu(21, 30);
                                        EffacerContenu(0, 2);

                                        AfficherEmplacementActuel("Voiture");

                                        Console.SetCursorPosition(18, 24);

                                        Console.Write($"{personnage.Prenom} part travailler");

                                        System.Threading.Thread.Sleep(3500);

                                        EffacerContenu(0, 2);
                                        EffacerContenu(21, 30);

                                        AfficherEmplacementActuel("Lieu de travail");

                                        Console.BackgroundColor = ConsoleColor.White;
                                        Console.SetCursorPosition(30 - ((echelon2 / 2) + 1), 24);

                                        for (int i = 0; i < echelon2 + 2; i++)
                                        {
                                            Console.Write(" ");
                                        }

                                        Console.SetCursorPosition(30 - ((echelon2 / 2) + 1), 25);
                                        Console.Write(" ");
                                        Console.SetCursorPosition(30 + ((echelon2 / 2)), 25);
                                        Console.Write(" ");

                                        Console.SetCursorPosition(30 - ((echelon2 / 2) + 1), 26);

                                        for (int i = 0; i < echelon2 + 2; i++)
                                        {
                                            Console.Write(" ");
                                        }

                                        while (travailEnCours)
                                        {
                                            pourcentage = Math.Round((progression / echelon2) * 100, 1);
                                            Console.BackgroundColor = ConsoleColor.Black;

                                            EffacerContenu(22, 24);

                                            Console.SetCursorPosition(19, 22);
                                            Console.Write($"PROGRESSION : {pourcentage} %");

                                            Console.SetCursorPosition(30 - ((echelon2 / 2)) + (int)progression, 25);
                                            Console.BackgroundColor = ConsoleColor.DarkGreen;

                                            System.Threading.Thread.Sleep(150);

                                            if (pourcentage == 100)
                                            {
                                                System.Threading.Thread.Sleep(3000);
                                                travailEnCours = false;
                                                choixOption = 0;
                                                Console.BackgroundColor = ConsoleColor.Black;
                                                EffacerContenu(21, 30);
                                            }

                                            else
                                            {
                                                progression++;

                                                Temps += 15;

                                                Console.Write(" ");
                                            }
                                        }

                                        EffacerContenu(0, 2);

                                        AfficherEmplacementActuel("Voiture");

                                        Console.SetCursorPosition(16, 24);

                                        Console.Write($"{personnage.Prenom} revient du travail");

                                        System.Threading.Thread.Sleep(3500);

                                        EffacerContenu(0, 30);

                                        personnage.Energie -= metier.CoutEnEnergie;
                                        personnage.Faim -= nombreAleatoire.Next(20, 31);
                                        personnage.Soif -= nombreAleatoire.Next(20, 31);
                                        personnage.Hygiene -= nombreAleatoire.Next(25, 51);
                                        personnage.Bonheur -= nombreAleatoire.Next(15, 31);

                                        salaireDuMois += metier.SalaireHoraire;

                                    }
                                }
                            }

                            else if (choixOption == 2)
                            {
                                //SE LAVER
                                EffacerContenu(21, 30);
                                Console.SetCursorPosition(23, 24);
                                Console.Write($"{personnage.Prenom} se lave");
                                System.Threading.Thread.Sleep(3000);
                                Temps += _SeLaver.TempsDeLAction;
                                personnage.Energie -= _SeLaver.CoutEnEnergie;

                                if (personnage.Energie > 100) personnage.Energie = 100;
                                else if (personnage.Energie < 0) personnage.Energie = 0;

                                personnage.Bonheur += _SeLaver.RapportBonheur;

                                if (personnage.Bonheur > 100) personnage.Bonheur = 100;
                                else if (personnage.Bonheur < 0) personnage.Bonheur = 0;

                                personnage.Hygiene = 100;
                                EffacerContenu(21, 30);
                            }

                            else if (choixOption == 3)
                            {
                                OuvrirLeFrigo(out ContenuFrigo, out personnage);
                            }

                            else if (choixOption == 4)
                            {
                                //DORMIR
                                choixOption = 20;

                                while (true)
                                {
                                    for (int i = 21; i < 30; i++)
                                    {
                                        Console.SetCursorPosition(21, i);
                                        Console.Write("                                      ");
                                    }

                                    Console.SetCursorPosition(30, 28);
                                    Console.Write("[ENTER] POUR VALIDER");

                                    Console.SetCursorPosition(30, 29);
                                    Console.Write("[ESCAPE] POUR QUITTER");

                                    if (choixOption == 20)
                                    {
                                        Console.SetCursorPosition(32, 25);
                                        Console.Write(" ÉNERGIE : + 15 ");

                                        Console.BackgroundColor = ConsoleColor.White;
                                        Console.ForegroundColor = ConsoleColor.Black;
                                    }

                                    Console.SetCursorPosition(25, 21);

                                    Console.Write(" 2 HEURES 30 ");

                                    Console.BackgroundColor = ConsoleColor.Black;
                                    Console.ForegroundColor = ConsoleColor.White;


                                    if (choixOption == 21)
                                    {
                                        Console.SetCursorPosition(32, 25);
                                        Console.Write(" ÉNERGIE : + 32 ");

                                        Console.BackgroundColor = ConsoleColor.White;
                                        Console.ForegroundColor = ConsoleColor.Black;
                                    }

                                    Console.SetCursorPosition(42, 21);

                                    Console.Write(" 4 HEURES ");

                                    Console.BackgroundColor = ConsoleColor.Black;
                                    Console.ForegroundColor = ConsoleColor.White;

                                    if (choixOption == 22)
                                    {
                                        Console.SetCursorPosition(32, 25);
                                        Console.Write(" ÉNERGIE : + 57 ");

                                        Console.BackgroundColor = ConsoleColor.White;
                                        Console.ForegroundColor = ConsoleColor.Black;
                                    }

                                    Console.SetCursorPosition(25, 23);

                                    Console.Write(" 6 HEURES 30 ");

                                    Console.BackgroundColor = ConsoleColor.Black;
                                    Console.ForegroundColor = ConsoleColor.White;

                                    if (choixOption == 23)
                                    {
                                        Console.SetCursorPosition(32, 25);
                                        Console.Write(" ÉNERGIE : + 80 ");

                                        Console.BackgroundColor = ConsoleColor.White;
                                        Console.ForegroundColor = ConsoleColor.Black;
                                    }

                                    Console.SetCursorPosition(42, 23);

                                    Console.Write(" 8 HEURES ");

                                    Console.BackgroundColor = ConsoleColor.Black;
                                    Console.ForegroundColor = ConsoleColor.White;

                                    touche = Console.ReadKey(true);

                                    if ((int)touche.Key == 39)
                                    {
                                        if (choixOption < 23)
                                        {
                                            choixOption++;
                                        }
                                    }

                                    if ((int)touche.Key == 37)
                                    {
                                        if (choixOption > 20)
                                        {
                                            choixOption--;
                                        }
                                    }

                                    if ((int)touche.Key == 38)
                                    {
                                        if (choixOption - 2 >= 20)
                                        {
                                            choixOption -= 2;
                                        }
                                    }

                                    if ((int)touche.Key == 40)
                                    {
                                        if (choixOption + 2 <= 23)
                                        {
                                            choixOption += 2;
                                        }
                                    }

                                    if ((int)touche.Key == 27)
                                    {
                                        choixOption = 4;
                                        break;
                                    }

                                    if ((int)touche.Key == 13)
                                    {
                                        if (choixOption == 20)
                                        {
                                            Temps += 150;
                                            personnage.Energie += 15;
                                        }

                                        else if (choixOption == 21)
                                        {
                                            Temps += 240;
                                            personnage.Energie += 32;
                                        }

                                        else if (choixOption == 22)
                                        {
                                            Temps += 390;
                                            personnage.Energie += 57;
                                        }

                                        else if (choixOption == 23)
                                        {
                                            Temps += 480;
                                            personnage.Energie += 80;
                                        }

                                        EffacerContenu(21, 30);

                                        Console.SetCursorPosition(23, 25);
                                        Console.Write($"{personnage.Prenom} dort");

                                        System.Threading.Thread.Sleep(4000);

                                        EffacerContenu(21, 30);

                                        personnage.Faim -= nombreAleatoire.Next(10, 21);
                                        personnage.Soif -= nombreAleatoire.Next(10, 21);
                                        personnage.Hygiene -= nombreAleatoire.Next(5, 16);

                                        if (personnage.Energie > 100)
                                        {
                                            personnage.Energie = 100;
                                        }

                                        if (personnage.Faim < 0)
                                        {
                                            personnage.Faim = 0;
                                        }

                                        if (personnage.Soif < 0)
                                        {
                                            personnage.Soif = 0;
                                        }

                                        if (personnage.Hygiene < 0)
                                        {
                                            personnage.Hygiene = 0;
                                        }

                                        choixOption = 4;
                                        break;
                                    }
                                }
                            }

                            else if (choixOption == 5)
                            {
                                vitesse *= -1;

                                if (vitesse == -1)
                                {
                                    vitesseWatch = 8;
                                }

                                if (vitesse == 1)
                                {
                                    vitesseWatch = 2;
                                }
                            }
                        }

                        //Rafraichir la fenêtre toutes les 15 secondes
                        if (stopwatch.Elapsed.Seconds >= vitesseWatch)
                        {
                            compteTour++;

                            if (compteTour == 4)
                            {
                                if (personnage.Sante == 0)
                                {
                                    //GAME OVER
                                    EffacerContenu(0, 30);
                                    Console.SetCursorPosition(22,13);
                                    Console.Write($"{personnage.Prenom} est mort.");
                                    Console.SetCursorPosition(16, 15);
                                    Console.Write($"{personnage.Prenom} a survécu {compteurDeJours} jour(s)");
                                    Console.SetCursorPosition(20, 17);
                                    Console.Write($"Merci d'avoir joué !");
                                    Console.SetCursorPosition(14, 21);
                                    Console.Write($"Appuyez sur [ENTER] pour quitter");

                                    while (true)
                                    {
                                        touche = Console.ReadKey(true);

                                        if ((int)touche.Key == 13)
                                        {
                                            break;
                                        }
                                    }

                                    Jeu = false;
                                    EcranActuel = "Menu";
                                }

                                personnage.Sante -= (4 - ((personnage.Faim + personnage.Soif + personnage.Hygiene + personnage.Energie + personnage.Bonheur) / 100));

                                if (personnage.Sante > 100) personnage.Sante = 100;
                                else if (personnage.Sante < 0) personnage.Sante = 0;

                                personnage.Bonheur -= (4 - ((personnage.Faim + personnage.Soif + personnage.Hygiene + personnage.Energie) / 80));

                                if (personnage.Bonheur > 100) personnage.Bonheur = 100;
                                else if (personnage.Bonheur < 0) personnage.Bonheur = 0;

                                personnage.Faim -= nombreAleatoire.Next(1, 9);

                                if (personnage.Faim > 100) personnage.Faim = 100;
                                else if (personnage.Faim < 0) personnage.Faim = 0;

                                personnage.Soif -= nombreAleatoire.Next(1, 9);

                                if (personnage.Soif > 100) personnage.Soif = 100;
                                else if (personnage.Soif < 0) personnage.Soif = 0;

                                personnage.Hygiene -= nombreAleatoire.Next(1, 6);

                                if (personnage.Hygiene > 100) personnage.Hygiene = 100;
                                else if (personnage.Hygiene < 0) personnage.Hygiene = 0;

                                personnage.Energie -= nombreAleatoire.Next(1, 4);

                                if (personnage.Energie > 100) personnage.Energie = 100;
                                else if (personnage.Energie < 0) personnage.Energie = 0;

                                compteTour = 0;
                            }

                            if (JourActuel == 1 && payeRecue == false)
                            {
                                EffacerContenu(21, 30);
                                Console.SetCursorPosition(14, 24);
                                Console.Write($"{personnage.Prenom} reçoit sa paye : {salaireDuMois} $");
                                System.Threading.Thread.Sleep(4000);
                                EffacerContenu(21, 30);
                                personnage.Argent += salaireDuMois;
                                salaireDuMois = 0;
                                payeRecue = true;
                            }

                            else if (JourActuel == 2)
                            {
                                payeRecue = false;
                            }

                            for (int i = 3; i < 14; i++)
                            {
                                Console.SetCursorPosition(22, i);
                                Console.Write("                              ");
                            }

                            

                            Temps += 15;
                            stopwatch = System.Diagnostics.Stopwatch.StartNew();
                            dir = 1;
                        }
                    }

                    Jeu = true;

                }
                
                Console.CursorVisible = false;
                Console.Clear();
            }
        }
    }
}
