﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Simulation_of_Life
{
    class Magasin
    {
        public Dictionary<Produit, int> Panier = new Dictionary<Produit, int>();

        public void AjouterAuPanier(Produit x)
        {
            if (Panier.ContainsKey(x))
            {
                
                Panier[x] += 1;
            }

            else
            {
                Panier.Add(x, 1);
            }
        }

        public void RetirerDuPanier(Produit x)
        {
            Panier[x] -= 1;

            if (Panier[x] == 0)
            {
                Panier.Remove(x);
            }
        }
    }
}
