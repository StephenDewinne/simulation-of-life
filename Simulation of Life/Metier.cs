﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Simulation_of_Life
{
    class Metier
    {
        private string _nomDuMetier;
        private double _salaireHoraire;
        private int _coutEnEnergie;
        private int _risqueDuMetier;
        private int _heureDeDebut;
        private int _heureDeFin;

        public string NomDuMetier { get; set; }
        public double SalaireHoraire { get; set; }
        public int CoutEnEnergie { get; set; }
        public int RisqueDuMetier { get; set; }
        public int HeureDeDebut { get; set; }
        public int HeureDeFin { get; set; }
    }
}
