﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Simulation_of_Life
{
    class Actions
    {
        public string Action { get; set; }
        public int CoutEnEnergie { get; set; }
        public double CoutEnArgent { get; set; }
        public int TempsDeLAction { get; set; }
        public int RapportBonheur { get; set; }
    }
}
