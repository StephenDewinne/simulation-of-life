﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Simulation_of_Life
{
    class Produit
    {
        public Produit(string nom, double prix, int iFaim, int iSoif, int iBonheur, int iEnergie)
        {
            Nom = nom;
            Prix = prix;
            ImpactFaim = iFaim;
            ImpactSoif = iSoif;
            ImpactBonheur = iBonheur;
            ImpactEnergie = iEnergie;
        }

        public string Nom { get; set; }
        public double Prix { get; set; }
        public int ImpactFaim { get; set; }
        public int ImpactSoif { get; set; }
        public int ImpactBonheur { get; set; }
        public int ImpactEnergie { get; set; }
    }
}
